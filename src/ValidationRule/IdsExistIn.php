<?php

namespace Infotechnohelp\Orm\ValidationRule;

use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class IdsExistIn
{

    protected $tableAlias;

    public function __construct($tableAlias)
    {
        $this->tableAlias = $tableAlias;
    }

    public function __invoke($value, $context)
    {
        $table = TableRegistry::getTableLocator()->get($this->tableAlias);

        if (!array_key_exists('_ids', $value)) {
            return true;
        }

        foreach ($value['_ids'] as $id) {
            if ($table->findById($id)->first() === null) {
                Log::error("{$this->tableAlias} id $id does not exist.");
                return false;
            }
        }
        return true;
    }
}