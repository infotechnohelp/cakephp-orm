<?php

namespace Infotechnohelp\Orm\Rule;

use Cake\Datasource\EntityInterface;

class RequireAtLeastOneOf
{

    protected $fields;

    public function __construct($fields)
    {
        $this->fields = $fields;
    }

    public function __invoke(EntityInterface $entity, array $options)
    {
        foreach ($this->fields as $field){
            if(!empty($entity->{$field})){
                return true;
            }
        }

        return false;
    }
}