<?php

namespace Infotechnohelp\Orm\Rule;

use Cake\Datasource\EntityInterface;

class RequireOnlyOneOf
{

    protected $fields;

    public function __construct($fields)
    {
        $this->fields = $fields;
    }

    public function __invoke(EntityInterface $entity, array $options)
    {
        $notEmpty = [];

        foreach ($this->fields as $field) {
            if (!empty($entity->{$field})) {
                $notEmpty[] = $field;
            }
        }

        if(count($notEmpty) === 1){
            return true;
        }

        return false;
    }
}