<?php

namespace Infotechnohelp\Orm\Rule;

use Cake\Datasource\EntityInterface;

class RequireAtLeastOneOfGroups
{

    protected $fieldGroups;

    public function __construct($fieldGroups)
    {
        $this->fieldGroups = $fieldGroups;
    }

    public function __invoke(EntityInterface $entity, array $options)
    {
        foreach ($this->fieldGroups as $fieldGroup){

            $result = true;

            foreach($fieldGroup as $field){
                if(empty($entity->{$field})){
                    $result = false;
                }
            }

            if($result){
                return true;
            }
        }

        return false;
    }
}