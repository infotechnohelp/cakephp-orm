<?php

namespace Infotechnohelp\Orm\Rule;

use Cake\Datasource\EntityInterface;

class NotEmpty
{

    protected $field;

    public function __construct(string $field)
    {
        $this->field = $field;
    }

    public function __invoke(EntityInterface $entity, array $options)
    {
        if (empty($entity->get($this->field))) {
            return false;
        }

        return true;
    }
}