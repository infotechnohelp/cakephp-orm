<?php

namespace Infotechnohelp\Orm\Rule;

use Cake\Datasource\EntityInterface;

class RequireOnlyOneOfGroups
{

    protected $fieldGroups;

    public function __construct($fieldGroups)
    {
        $this->fieldGroups = $fieldGroups;
    }

    public function __invoke(EntityInterface $entity, array $options)
    {
        $notEmptyFieldGroups = 0;

        foreach ($this->fieldGroups as $fieldGroup){

            $result = true;

            foreach($fieldGroup as $field){
                if(empty($entity->{$field})){
                    $result = false;
                }
            }

            if($result){
                $notEmptyFieldGroups++;
            }
        }

        if($notEmptyFieldGroups === 1){
            return true;
        }

        return false;
    }
}